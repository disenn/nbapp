//
//  NbappTests.swift
//  NbappTests
//
//  Created by Franck Dosso on 11/2/20.
//

import XCTest
@testable import Nbapp

class NbappTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    
    func testPlayerFullName() {
        
        let team = Team(id: 0, abbreviation: "LA", city: "Los Angeles", conference: "West", division: "", full_name: "Los Angeles Lakers", name: "Lakers")
        var player = Player(id: 1, first_name: "Franck", last_name: "Dosso", team: team, position: "G")

        
        XCTAssertEqual(player.full_name(), "Franck Dosso", "Correct!")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
