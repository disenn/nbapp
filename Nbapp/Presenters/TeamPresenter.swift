//
//  TeamPresenter.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import Foundation
protocol TeamView {
    func retrieveItems(isSearching: Bool, teams: [Team])
    func displayError(error: String)
    func startSpinner()
    func stopSpinner()
}

class TeamPresenter {
    
    var view: TeamView?
    var teams = [Team]()
    
    init(view: TeamView) {
        self.view = view
    }
    
    //Call to retrieve teams
    func start() {
        self.view?.startSpinner()
        Service.shared.getTeams { (teams, error) in
            self.view?.stopSpinner()
            if let teams = teams {
                self.teams = teams.data
                self.view?.retrieveItems(isSearching: false, teams: teams.data)
            } else {
                self.view?.displayError(error: error ?? "Unknown error")
            }
        }
    }
    //refresh VC with retained data
    func refresh() {
        self.view?.retrieveItems(isSearching: false, teams: self.teams)
    }
    
    //filter teams by team's name
    func searchTeams(text: String, teams: [Team]) {
        let filteredTeams = teams.filter({$0.full_name.contains(text)})
        self.view?.retrieveItems(isSearching: true, teams: filteredTeams)
    }
}
