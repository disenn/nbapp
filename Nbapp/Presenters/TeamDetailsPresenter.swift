//
//  TeamDetailsPresenter.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import Foundation

protocol PlayerView {
    func retrieveItems(players: [Player])
    func displayError(error: String)
    func displayEmptyState()
    func startSpinner()
    func stopSpinner()
}

class TeamDetailsPresenter  {
    var view: PlayerView?
    var team: Team?
    
    init(view: PlayerView, team: Team) {
        self.view = view
        self.team = team
    }
    
    func getTeam() -> Team? {
        return self.team
    }
    
    //Get All players and filter it by the team ID
    func start() {
        self.view?.startSpinner()
        Service.shared.getPlayers { (players, error) in
            self.view?.stopSpinner()
            if let players = players {
                let teamPlayers = players.data.filter({$0.team.id == self.team?.id})
                if teamPlayers.count == 0 {
                    self.view?.displayEmptyState()
                    return
                }
                self.view?.retrieveItems(players: teamPlayers)
            } else {
                self.view?.displayError(error: error ?? "Unknown error")
            }
        }
    }
}
