//
//  Service.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import Foundation
import UIKit

class Service {
    static let shared = Service()
    typealias TeamsCompletion = (_ response: Teams?, _ error: String?) -> Void
    typealias PlayersCompletion = (_ response: Players?, _ error: String?) -> Void

    
    func getTeams(completion: @escaping TeamsCompletion) {
        NbappDomain.shared.getTeams { (teams, error) in
            if let teams = teams {
                completion(teams, nil)
                return
            }
            completion(nil, error)
        }
    }
    
    func getPlayers(completion: @escaping PlayersCompletion) {
        NbappDomain.shared.getPlayers { (players, error) in
            if let players = players {
                completion(players, nil)
                return
            }
            completion(nil, error)
        }
    }
}
