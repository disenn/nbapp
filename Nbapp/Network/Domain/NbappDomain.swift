//
//  NbappDomain.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import Foundation
import Alamofire

class NbappDomain {
    
    static let shared = NbappDomain()
    let baseURL = "https://rapidapi.p.rapidapi.com/"
    typealias TeamsCompletion = (_ response: Teams?, _ error: String?) -> Void
    typealias PlayersCompletion = (_ response: Players?, _ error: String?) -> Void

    
    
    func getTeams(completion: @escaping TeamsCompletion) {
        let url = baseURL + "teams"
        let headers: HTTPHeaders = [
            "x-rapidapi-key": "806509c5e9msh499c32366bdb02fp1860b9jsnc30d048bdc03",
            "x-rapidapi-host": "free-nba.p.rapidapi.com"
        ]
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseDecodable { (response: DataResponse<Teams, AFError>) in
            if let error = response.error {
                completion(nil, error.localizedDescription)
                return
            }
            switch response.result {
            case .success(let value):
                completion(value, nil)
            case .failure(let error):
                completion(nil, error.localizedDescription)
                break
            }
        }
    }
    
    func getPlayers(completion: @escaping PlayersCompletion) {
        let url = baseURL + "players"
        let headers: HTTPHeaders = [
            "x-rapidapi-key": "806509c5e9msh499c32366bdb02fp1860b9jsnc30d048bdc03",
            "x-rapidapi-host": "free-nba.p.rapidapi.com"
        ]
        let parameters = ["per_page": "100"]
        AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseDecodable { (response: DataResponse<Players, AFError>) in
            if let error = response.error {
                completion(nil, error.localizedDescription)
                return
            }
            switch response.result {
            case .success(let value):
                completion(value, nil)
            case .failure(let error):
                completion(nil, error.localizedDescription)
                break
            }
        }
    }
}
