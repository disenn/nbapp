//
//  PlayerDetailViewController.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/5/20.
//

import UIKit
import Lottie

class PlayerDetailViewController: UIViewController, ControllerView {
    
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var firstLabel: UILabel!
    
    @IBOutlet weak var lastLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    var player: Player?
    @IBOutlet weak var titleLabel: UILabel!
    
    
    static func createVC(player: Player) -> PlayerDetailViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "playerDetailsVC") as! PlayerDetailViewController
        vc.player = player
        return vc
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.animate()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    private func animate() {
        self.animationView.animation = Animation.named(AppConstants.dribbleAnimation)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.play { (play) in
            if !play {
                self.animationView.forceDisplayUpdate()
                self.animationView.play()
            }
        }
        
        UIView.animate(withDuration: 2, animations: {
            //self.logoView.alpha = 1
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func setupUI() {
        lastLabel.text = "Last name: \(player?.last_name ?? "")"
        firstLabel.text = "First name: \(player?.first_name ?? "")"
        positionLabel.text = player?.position == "" ? "Position: Unknown" : "Position: \(player?.position ?? "Unknown")"
        titleLabel.text = "\(player?.first_name ?? "")'s highlights"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
