//
//  TeamsViewController.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/2/20.
//

import UIKit

class TeamsViewController: UIViewController, ControllerView {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var searchBarView: UISearchBar!
    private let spacing:CGFloat = 16.0
    var teams: [Team]?
    var searchedTeams: [Team]?
    var presenter: TeamPresenter?
    var searching = false
    var spinner: UIActivityIndicatorView?


    //Initiate View controller and presenter
    static func createVC() -> TeamsViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "teamsVC") as! TeamsViewController
        vc.presenter = TeamPresenter(view: vc)
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.presenter?.start()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        //set up collection View
        let cell = UINib(nibName: "TeamsCell", bundle: nil)
        collectionView.register(cell, forCellWithReuseIdentifier: "teamsCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        collectionView.collectionViewLayout = layout
        searchBarView.delegate = self
        //setup spinner loading
        spinner = UIActivityIndicatorView(style: .large)
        spinner?.center = self.view.center
        spinner?.color = AppConstants.themeGreen
        if let view = spinner {
            self.view.addSubview(view)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension TeamsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searching {
            return searchedTeams?.count ?? 0
        }
        return teams?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "teamsCell", for: indexPath) as! TeamsCell
        let team = searching ? searchedTeams : teams
        cell.display(team: team?[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 16
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        //Amount of total spacing in a row
        let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let teams = searching ? searchedTeams : self.teams
        guard let team = teams?[indexPath.row] else {
            return
        }
        let vc = TeamDetailsViewController.createVC(team: team)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}
extension TeamsViewController: TeamView {
    func startSpinner() {
        spinner?.startAnimating()
    }
    
    func stopSpinner() {
        spinner?.stopAnimating()
    }
    
    func retrieveItems(isSearching: Bool, teams: [Team]) {
        searching = isSearching
        if isSearching {
            searchedTeams = teams
            self.collectionView.reloadData()
            return
        }
        self.teams = teams
        self.collectionView.reloadData()
    }
    
    func displayError(error: String) {
        let vc = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Okay", style: .cancel, handler: nil)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { (_) in
            self.presenter?.start()
        }
        vc.addAction(okAction)
        vc.addAction(retryAction)
        self.present(vc, animated: true, completion: nil)
    }
    }

extension TeamsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let teams = self.teams else {
            return
        }
        if searchText == "" || searchText.isEmpty {
            self.presenter?.refresh()
            return
        }
        self.presenter?.searchTeams(text: searchText, teams: teams)
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.presenter?.refresh()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

