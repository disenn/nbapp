//
//  ViewController.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/2/20.
//

import UIKit
import Lottie

class ViewController: UIViewController, ControllerView {
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var startButton: UIButton!
    
    @IBAction func startAction(_ sender: Any) {
        gotoTeamsVC()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        animate()
    }
    
    func setupUI() {
        startButton.layer.cornerRadius = startButton.frame.height / 2
        startButton.backgroundColor = AppConstants.themeGreen
    }
    
    private func animate() {
        self.animationView.animation = Animation.named(AppConstants.jumpAnimation)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .repeatBackwards(1.0)
        animationView.play { (play) in
            if !play {
                self.animationView.forceDisplayUpdate()
                self.animationView.play()
            }
        }
        
        UIView.animate(withDuration: 2, animations: {
            self.logoView.alpha = 1
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func gotoTeamsVC() {
        let vc = TeamsViewController.createVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}

