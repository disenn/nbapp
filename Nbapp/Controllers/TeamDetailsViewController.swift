//
//  TeamDetailsViewController.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import UIKit

class TeamDetailsViewController: UIViewController, ControllerView {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    private let spacing:CGFloat = 16.0
    var presenter: TeamDetailsPresenter?
    var players: Players?
    var teamPlayers: [Player]?
    @IBOutlet weak var emptyStateLabel: UILabel!
    var spinner: UIActivityIndicatorView?

    

    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //initiate VC and presenter
    static func createVC(team: Team) -> TeamDetailsViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "teamDetailsVC") as! TeamDetailsViewController
        vc.presenter = TeamDetailsPresenter(view: vc, team: team)
        return vc
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.presenter?.start()
        // Do any additional setup after loading the view.
    }
    
    //setup collection view and spinner loading
    func setupUI() {
        let cell = UINib(nibName: "TeamsCell", bundle: nil)
        let headerCell = UINib(nibName: "HeaderCell", bundle: nil)
        collectionView.register(cell, forCellWithReuseIdentifier: "teamsCell")
        collectionView.register(headerCell, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 20, right: 20)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        collectionView.collectionViewLayout = layout
        spinner = UIActivityIndicatorView(style: .large)
        spinner?.center = self.view.center
        spinner?.color = AppConstants.themeGreen
        if let view = spinner {
            self.view.addSubview(view)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TeamDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.teamPlayers?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "teamsCell", for: indexPath) as! TeamsCell
        cell.display(player: self.teamPlayers?[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 16
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        //Amount of total spacing in a row
        let width = (collectionView.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard  let player = self.teamPlayers?[indexPath.row] else {
            return
        }
        let vc = PlayerDetailViewController.createVC(player: player)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderCell.identifier, for: indexPath) as! HeaderCell
        if let team = self.presenter?.getTeam() {
            cell.display(team: team)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 235)
    }
    
}
extension TeamDetailsViewController: PlayerView {
    func displayEmptyState() {
        emptyStateLabel.alpha = 1
    }
    
    func startSpinner() {
        spinner?.startAnimating()
    }
    
    func stopSpinner() {
        spinner?.stopAnimating()
    }
    
    func retrieveItems(players: [Player]) {
        emptyStateLabel.alpha = 0
        self.teamPlayers = players
        collectionView.reloadData()
    }
    
    func displayError(error: String) {
        let vc = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Okay", style: .cancel, handler: nil)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { (_) in
            self.presenter?.start()
        }
        vc.addAction(okAction)
        vc.addAction(retryAction)
        self.present(vc, animated: true, completion: nil)
    }
    
    
}

