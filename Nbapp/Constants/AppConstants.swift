//
//  AppConstants.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/2/20.
//

import Foundation
import UIKit

class AppConstants {
    static var jumpAnimation = "jump"
    static var dribbleAnimation = "dribble"
    static let themeGreen = UIColor(hexString: "#02733E")
    static let themeYellow = UIColor(hexString: "#F2BE22")

}
