//
//  HeaderCell.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import Foundation
import UIKit

class HeaderCell: UICollectionReusableView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var conferenceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    static var identifier = "headerCell"
    
    func display(team: Team) {
        cityLabel.text = "City: \(team.city)"
        conferenceLabel.text = "Conference: \(team.conference)"
        nameLabel.text = "Name: \(team.name)"
        imageView.round()
                
    }
}
