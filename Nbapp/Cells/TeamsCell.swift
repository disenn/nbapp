//
//  TeamsCell.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/3/20.
//

import Foundation
import UIKit
class TeamsCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    //display cell for team View
    func display(team: Team?) {
        imageView.round()
        if team?.conference.uppercased() == "EAST" {
            imageView.image = UIImage(named: "east")
        } else {
            imageView.image = UIImage(named: "west")
        }
        self.backgroundView?.backgroundColor = AppConstants.themeYellow
        titleLabel.text = team?.full_name
    }
    
    //display cell for team details View
    func display(player: Player?) {
        imageView.round()
        imageView.image = UIImage(named: "player")
        self.backgroundView?.backgroundColor = AppConstants.themeYellow
        titleLabel.text = player?.first_name 
    }
}
