//
//  ControllerView.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/5/20.
//

import Foundation

protocol ControllerView {
    func setupUI()
}
