//
//  Player.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import Foundation
import UIKit

struct Players: Decodable {
    
    var data: [Player]
    
    enum CodingKeys: String, CodingKey {
        case data
    }

}
struct Player: Decodable {
    
    let id: Int
    let first_name: String
    let last_name: String
    let team: Team
    let position: String

    
    enum CodingKeys: String, CodingKey {
        case id
        case first_name
        case last_name
        case team
        case position
    }
    
}

extension Player {
    mutating func full_name() -> String {
        let fullName = "\(first_name) \(last_name)"
        return fullName
    }
}
