//
//  Team.swift
//  Nbapp
//
//  Created by Franck Dosso on 11/4/20.
//

import Foundation
import UIKit

struct Teams: Decodable {
    
    var data: [Team]
    
    enum CodingKeys: String, CodingKey {
        case data
    }

}
struct Team: Decodable {
    
    let id: Int
    let abbreviation: String
    let city: String
    let conference: String
    let division: String
    let full_name: String
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case abbreviation
        case city
        case conference
        case division
        case full_name
        case name
    }
    
}
class Product: NSObject {
    var total = 0
    init(num: Int) {
        total = num
    }
    func multiplyBy2() -> Int {
        let result = total * 2
        return result
    }
}
